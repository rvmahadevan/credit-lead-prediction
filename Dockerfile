FROM python:3.8
MAINTAINER Mahadevan Varadhan


RUN mkdir -p /usr/local/datascience/ 

# Create app directory
WORKDIR /usr/local/datascience/

# Install app dependencies
COPY /src/ /usr/local/datascience/src/
COPY requirements.txt requirements.txt

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

USER root

# Spark dependencies
# Default values can be overridden at build time
# (ARGS are in lower case to distinguish them from ENV)
ARG spark_version="3.1.1"
ARG hadoop_version="3.2"
ARG spark_checksum="E90B31E58F6D95A42900BA4D288261D71F6C19FA39C1CB71862B792D1B5564941A320227F6AB0E09D946F16B8C1969ED2DEA2A369EC8F9D2D7099189234DE1BE"
ARG openjdk_version="11"

ENV APACHE_SPARK_VERSION="${spark_version}" \
    HADOOP_VERSION="${hadoop_version}"

RUN apt-get update --yes && \
    apt-get install --yes --no-install-recommends \
    "openjdk-${openjdk_version}-jre-headless" \
    ca-certificates-java && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

# Spark installation
WORKDIR /tmp
RUN wget -q "https://archive.apache.org/dist/spark/spark-${APACHE_SPARK_VERSION}/spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz" && \
    echo "${spark_checksum} *spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz" | sha512sum -c - && \
    tar xzf "spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz" -C /usr/local --owner root --group root --no-same-owner && \
    rm "spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz"

WORKDIR /usr/local

# Configure Spark
ENV SPARK_HOME=/usr/local/spark
ENV SPARK_OPTS="--driver-java-options=-Xms1024M --driver-java-options=-Xmx4096M --driver-java-options=-Dlog4j.logLevel=info" \
    PATH=$PATH:$SPARK_HOME/bin
ENV CONDA_DIR = /usr/local/conda

RUN ln -s "spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}" spark && \
    # Add a link in the before_notebook hook in order to source automatically PYTHONPATH
    mkdir -p /usr/local/bin/before-notebook.d && \
    ln -s "${SPARK_HOME}/sbin/spark-config.sh" /usr/local/bin/before-notebook.d/spark-config.sh

# Fix Spark installation for Java 11 and Apache Arrow library
# see: https://github.com/apache/spark/pull/27356, https://spark.apache.org/docs/latest/#downloading
RUN cp -p "$SPARK_HOME/conf/spark-defaults.conf.template" "$SPARK_HOME/conf/spark-defaults.conf" && \
    echo 'spark.driver.extraJavaOptions -Dio.netty.tryReflectionSetAccessible=true' >> $SPARK_HOME/conf/spark-defaults.conf && \
    echo 'spark.executor.extraJavaOptions -Dio.netty.tryReflectionSetAccessible=true' >> $SPARK_HOME/conf/spark-defaults.conf

# USER $NB_UID

# Install pyarrow
# RUN conda install --quiet --yes --satisfied-skip-solve \
#     'pyarrow=4.0.*' && \
#     conda clean --all -f -y && \
#     fix-permissions "${CONDA_DIR}" && \
#     fix-permissions "/home/local"

WORKDIR /usr/local/datascience/


RUN pip install --upgrade --no-cache-dir -r requirements.txt

ENV DATASCIENCE_HOME=/usr/local/datascience

RUN pip install --upgrade pip

RUN pip install -U Flask && \
    pip install Flask-RESTful && \
    pip install Flask-JWT && \
    pip install flask-bcrypt && \
    pip install Flask-Migrate && \
    pip install flask_login && \
    pip install pandas && \
    pip install nltk && \
    pip install sqlalchemy && \
    pip install psycopg2-binary && \
    pip install --upgrade werkzeug  && \
    pip install pyyaml  && \
    pip install pymysql && \
    pip install cryptography && \
    pip install simplejson  && \
    pip install jupyter && \
    pip install vaex && \
    pip install pyarrow && \
    pip install xlrd && \
    pip install openpyxl && \
    pip install pyspark && \
    pip install seaborn && \
    pip install sklearn && \
    pip install pandas_profiling && \
    pip install sweetviz && \
    pip install autoviz && \
    pip install category_encoders &&\
    pip install pycaret

# RUN pip install awscli --force-reinstall --upgrade
RUN apt-get install texlive texlive-latex-extra pandoc -y
RUN apt-get update
RUN apt-get install vim -y

# ENTRYPOINT ["python"]
CMD ["jupyter", "notebook", "--port=9090", "--no-browser", "--ip=0.0.0.0", "--allow-root"]

EXPOSE 9090 

